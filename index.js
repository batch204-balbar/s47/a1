// Directions:
// Create two event listeners when a user types in the first and last name inputs
// When this event triggers, update the span-full-name's content to show the value of the last name input on the right
// Stretch goal: Instead of an anonymous function, create a new function that the two event listeners will call
// Guide question: Where do the names come from and where should they go?

let inputFirstName = document.querySelector('#txt-first-name');
let inputLastName = document.querySelector('#txt-last-name');
let fullName = document.querySelector('#span-full-name');

let generateFullName = (e) => {
	fullName.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
};

inputFirstName.addEventListener('keyup',generateFullName);
inputLastName.addEventListener('keyup',generateFullName);